const { Restaurant, Shifts, File } = require("../models").models;
const sequelize =  require('../models').sequelize
const { Op } = require('sequelize');
const { join } = require('path')
const { createReadStream } = require('fs')
const csv2json = require('csvtojson');
const filesDir = join(__dirname,'..','files')

const days = ['mon','tue','wed','thu','fri','sat','sun']
const daysRegex = new RegExp('mon|tue|wed|thu|fri|sat|sun','i')

const readCsvToJson = async function (filename){
    //Find a row that matches the query,save the row if none is found (wont work under a transaction)
    const [file, notReadBefore] = await File.findCreateFind({
        where:{
            name:filename
        }
    })
    // check if the file is already read and parsed
    if(notReadBefore){
        filename = join(filesDir,filename)
        const readStream = createReadStream(filename);
        const json = await  csv2json({
            ignoreEmpty:true,
            noheader:true,
            headers:['name','shifts']
        })
        .fromStream(readStream)
        return json
    }
    return []
}

const insertRestaurant = async function(name){
    const restaurant = await Restaurant.create({name})
    return restaurant.id
}

const bulkInsertShifts = async function(list){
    return await Shifts.bulkCreate(list)
}

const parseShifts = async function(shiftText,restaurantId){
    let shifts = shiftText.split('/')
    let parsedShifts = []
    for (let i = 0; i < shifts.length; i++) {
        const e = shifts[i];
        let anotherDaySameShift = null
        let addSingleDayShift = false
        let singlaDayShift = null
        
        let shift = e.split(' ').filter(e=>e.length>0).map(e=>e.toLowerCase())

        // handle flipped shifts (single day before range), example: Mon, Wed-Sun 11 am - 10 pm
        let testFlipped = shift[0].split(',') 
        if(days.includes(testFlipped[0]) && testFlipped.length > 1){
            let tmp = testFlipped[0]
            shift[0] = shift[1]
            shift[1] = tmp
        }
        
        let fromTo = shift[0].split('-')

        // single day shift, example: Sun 11 am - 10 pm
        let fromDay = fromTo[0]
        let toDay = fromTo[0]

        // days range shift, example: Mon-Fri 11 am - 10 pm
        if (fromTo.length == 2){
            fromDay = fromTo[0].match(daysRegex)[0] 
            toDay = fromTo[1].match(daysRegex)[0]
        }
        
        // Get the day number according to the days array - to be used to query day range    
        fromDay = days.indexOf(fromDay)
        toDay = days.indexOf(toDay)
    
        // one range of days example: Mon-Wed 5 pm - 12:30 am 
        let rangeStart = shift[1]
        let startDayPart = shift[2] 
        let rangeEnd = shift[4]
        let endDayPart = shift[5] 
    
        //single day same time range, example: Mon-Thu, Sun 11:30 am - 10 pm
        if(days.includes(shift[1])){
            rangeStart = shift[2] 
            startDayPart = shift[3] 
            rangeEnd = shift[5]
            endDayPart = shift[6] 
            addSingleDayShift = true
            singlaDayShift = days.indexOf(shift[1])
        }
        
        // format the time of the shift as postgres TIME type
        if(!rangeStart.includes(':')) rangeStart += ':00'
        if(!rangeEnd.includes(':')) rangeEnd +=':00'
        rangeStart += ' '+ startDayPart
        rangeEnd += ' '+ endDayPart

        let pgShift = {
            restaurantId,
            fromTime: rangeStart,
            toTime: rangeEnd,
            //makde as integer inclusive range
            daysRange: sequelize.literal(`int4range('${fromDay}','${toDay}','[]')`) 
        }
        parsedShifts.push(pgShift)
        if(addSingleDayShift){
            anotherDaySameShift = {
                restaurantId,
                fromTime: rangeStart,
                toTime: rangeEnd,
                //makde as integer inclusive range
                daysRange: sequelize.literal(`int4range('${singlaDayShift}','${singlaDayShift}','[]')`) 
            }
            parsedShifts.push(anotherDaySameShift)
        }    
    }
    return parsedShifts
}

const openRestaurants = async function(date){
    date = new Date(date)
    let time = `${date.getHours()}:${date.getMinutes()}:00`
    let dayOfWeek = date.getDay() - 1
    if(dayOfWeek < 0) dayOfWeek = 6 //handle sunday 
    const result = await Restaurant.findAll({
        include:[
            {
                model: Shifts,
                where:{
                    daysRange:{
                        [Op.contains]:dayOfWeek
                    },
                    fromTime:{
                        [Op.lte]:time
                    },
                    toTime:{
                        [Op.gt]:time
                    },
                }
            }
        ]
    })
    return result;
}

module.exports = {
    readCsvToJson,
    insertRestaurant,
    parseShifts,
    bulkInsertShifts,
    openRestaurants,
}