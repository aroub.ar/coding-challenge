FROM node:14.17.6 AS build

USER node
RUN mkdir -p /home/node/app && chown -R node:node /home/node/app
WORKDIR /home/node/app

# Copy dependency information and install all dependencies
COPY --chown=node:node package.json package-lock.json ./

RUN npm install

# Copy source code (and all other relevant files)
COPY --chown=node:node . .

# Run-time stage
FROM node:14.17.6-alpine

# Set non-root user and expose port 8080
USER node
EXPOSE 8080

RUN mkdir -p /home/node/app && chown -R node:node /home/node/app
WORKDIR /home/node/app

# Create log directory
RUN mkdir -p /home/node/app/files && chown -R node:node /home/node/app/files

# Copy results from previous stage
COPY --chown=node:node --from=build /home/node/app/ .

CMD [ "npm", "start" ]