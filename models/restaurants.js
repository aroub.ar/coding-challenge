'use strict';
const Sequelize = require('sequelize');
class Restaurant extends Sequelize.Model {
  static init(sequelize, DataTypes) {
    return super.init(
      {
        id:{
          type:Sequelize.INTEGER,
          primaryKey:true,
          autoIncrement: true,
        },
        name:{
          type:Sequelize.TEXT,
          allowNull:false
        },
      },
      {
        tableName:'restaurants',
        modelName:'Restaurant',
        timestamps:true,
        paranoid: true,
        freezeTableName:true,
        underscored:false,
        indexes: [],
        sequelize
      }
    )};
    static associate(models) {
      this.hasMany(models.Shifts, {
        // onDelete: "CASCADE",
        foreignKey: 'restaurantId'
      });
    }
}
module.exports = Restaurant
