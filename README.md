# Coding challange

Coding challange single api using NodeJS, Express framework.

---
## Requirements

For development, you will only need Node.js and a node global package, NPM, installed in your environement.

## Install

    $ git clone https://gitlab.com/aroub.ar/coding-challange.git
    $ cd coding-challange
    $ npm install

## Configure app

Copy `./.env.example` to `./.env` then edit it with your settings. You will need: PostgreSQL Database:

- PG_USERNAME;
- PG_PASSWORD;
- PG_DB;
- PG_HOST;


## Running the project

    $ npm start

## Watch for development
You will need nodemon package installed globally

    $ npm install -g nodemon
    $ npm run watch

## Try it!

Run the following commands after copying `./.env.example` to `./.env` and filling it with your custom values
docker & docker-compose are needed

    $ docker-compose up -d


## Try it without Docker!

You will need PostgreSQL server running, fill its connection data into `.env` file.

Then execute:

    $ npm install
    $ npm start

## Test the output

Copy the formatted csv file to `./files/` directory, then run the project, call `/api/` endpoint

which takes the following as query params

- date (the date to query, default is current date/time)
- filename (the file name you want to parse and query, default is 'test.csv')
