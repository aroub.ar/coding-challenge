const express = require('express');
const router = express.Router();
const functions = require('./functions')

router.get('/',async(req,res,next)=>{
    try {
        let filename = req.query.filename || 'test.csv'
        let date = req.query.date || new Date() // default check open restaurant for today
        let data = await functions.readCsvToJson(filename)
        if(data.length > 0){
            for (let i = 0; i < data.length; i++) {
                const element = data[i];
                const restaurantId = await functions.insertRestaurant(element.name)
                const parsedShifts = await functions.parseShifts(element.shifts,restaurantId)
                await functions.bulkInsertShifts(parsedShifts)
            }        
        }
        const result = await functions.openRestaurants(date)
        res.status(200).send(result)
    } catch (error) {
        next(error)
    }
})

module.exports = router;
