'use strict';
const Sequelize = require('sequelize');
class Shifts extends Sequelize.Model {
  static init(sequelize, DataTypes) {
    return super.init(
      {
        id:{
          type:Sequelize.INTEGER,
          primaryKey:true,
          autoIncrement: true,
        },
        restaurantId:{
          type:Sequelize.INTEGER,
          allowNull:false
        },
        fromTime:{
          type:Sequelize.TIME,
          allowNull:false
        },
        toTime:{
          type:Sequelize.TIME,
          allowNull:false
        },
        daysRange:{
          type:Sequelize.RANGE(Sequelize.INTEGER),
          allowNull:false
        },
      },
      {
        tableName:'shifts',
        modelName:'Shifts',
        timestamps:true,
        paranoid: true,
        freezeTableName:true,
        underscored:false,
        sequelize
      }
    )};
    static associate(models) {
      this.belongsTo(models.Restaurant,{
        foreignKey:'restaurantId',
        targetKey:'id'
      })
    }
}
module.exports = Shifts
