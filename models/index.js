/**
  index.js is an import utility that grabs all models in the same folder,
  and instantiate a Sequelize object once for all models (instead of for each model).
  This is done by passing the single Sequelize object to each
  model as a reference, which each model then piggy-backs (sequelize.define())
  for creating a single db class model.
*/

"use strict";

const fs = require("fs");
const path = require("path");
const Sequelize = require("sequelize"); // Sequelize is a constructor
const config = {
    dialect: "postgres",
    username: process.env.PG_USERNAME || 'postgres',
    password: process.env.PG_PASSWORD,
    db: process.env.PG_DB,
    host: process.env.PG_HOST,
    port: process.env.PG_PORT || 5432,
} 
const db = {};

let sequelize;
sequelize = new Sequelize(config.db, config.username, config.password, config);

sequelize.authenticate().then(() => {
  console.log('Sequelize Connection established successfully.');
}).catch(err => {
  console.log('Unable to connect to the database:','warn');
  console.error(err)
})
// Load each model file
try {
  const models = Object.assign({}, ...fs.readdirSync(__dirname)
    .filter(file =>
      (file.indexOf(".") !== 0) && (file !== "index.js") && (file !== 'hooks.js')
    )
    .map(function (file) {
      const model = require(path.join(__dirname, file));
      return {
        [model.name]: model.init(sequelize),
      };
    })
  );
  // Load model associations
  for (const model of Object.keys(models)) {
    typeof models[model].associate === 'function' && models[model].associate(models);
  }
  console.log('Sequelize models loaded successfully');
  
} catch (err) {
  console.log('Unable to load Sequelize models:','warn');
  console.error(err)
}

db.sequelize = sequelize;
db.models = sequelize.models;

module.exports = db;