'use strict';
const Sequelize = require('sequelize');
class File extends Sequelize.Model {
  static init(sequelize, DataTypes) {
    return super.init(
      {
        id:{
          type:Sequelize.INTEGER,
          primaryKey:true,
          autoIncrement: true,
        },
        name:{
          type:Sequelize.TEXT,
          allowNull:false
        },
        read:{
          type:Sequelize.BOOLEAN,
          allowNull:false,
          defaultValue: true
        }
      },
      {
        tableName:'files',
        modelName:'File',
        timestamps:true,
        paranoid: true,
        freezeTableName:true,
        underscored:false,
        indexes: [
          {
            name:'unique_file',
            fields:['name'],
            unique:true
          }
        ],
        sequelize
      }
    )};
    static associate(models) {
    }
}
module.exports = File
