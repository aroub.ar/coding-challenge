'use strict';
const path = require('path');
const dotenv = require('dotenv')
const express = require('express');
dotenv.config({
    path: path.join(__dirname, './.env'),
})

/**
 * Sync Sequelize Models with alter flag
 */

const sequelize =  require('./models').sequelize //Important to load sequelize models

sequelize.sync({alter:true, logging:false})
.then((r)=>{
  console.log("done syncing");
})
.catch((error)=>{
  console.error(error);
})

const api = require('./controllers')

let app = express();

app.use(express.json({ limit: '5mb' }));
app.use(express.urlencoded({ extended: false }));
app.use('/api', api);

//welcome route, indicate the api is live
app.all('/api/v1/welcome',function (req, res) {
  res.status(200).send({message:'Welcome !!'})
});

// simple error handler
app.use(function (err, req, res, next) {
    console.error(err);
    res.status(500).send({message: 'Somthing went wrong'})
});

process.on('unhandledRejection', error => {
  console.log('Unhandeld rejection','warn')
  console.error(error)
})
process.on('uncaughtException', error => {
  console.log('Uncaught Exception','warn')
  console.error(error)
});

module.exports = app;
